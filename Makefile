all:
	python src/main.py
reset:
	rm -rf results/
clean:
	rm -f src/*.pyc
plot:
	python src/plotter.py

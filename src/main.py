#!/usr/bin/env python

import data
import em
import kmeans
import sys

k = 2
PATH_TO_DATA = "data/dist2.txt"
PATH_TO_OUTPUT = "results"

def main(argv):
   # get data from data/dist2.txt
   input_data = data.get(PATH_TO_DATA)
   if input_data:
      # run k-means
      #kmeans.run(input_data, k, PATH_TO_OUTPUT)
      # run em
      em.run(input_data, k, PATH_TO_OUTPUT)
   else:
      print("No input.")
   return True

if __name__ == "__main__":
   try:
      main(sys.argv[1:])
   except KeyboardInterrupt:
      print("KeyboardInterrupt-ed.") 
      sys.exit(1)
   else:
      sys.exit(0)

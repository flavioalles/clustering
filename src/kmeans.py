#!/usr/bin/env python

import copy
import math
import os.path

def get_distance(p1, p2):
   distance = 0
   for values in zip(p1, p2):
      distance += pow(values[0] - values[1], 2)
   distance = math.sqrt(distance)
   return distance

def designate_cluster(point, means):
   min_distance = None
   for mean_index,mean in enumerate(means):
      distance = get_distance(point, mean)
      if min_distance == None or distance < min_distance:
         min_distance = distance
         cluster_index = mean_index 
   return cluster_index

def recalculate_means(clusters, input_data):
   means = list()
   for cluster in clusters:
      totals = [0, 0]
      for point_index in cluster: 
         totals[0] += input_data[point_index][0] 
         totals[1] += input_data[point_index][1] 
      means.append([totals[0]/len(cluster), totals[1]/len(cluster)])
   return means

def output_results(iterations, means, clusters, input_data, path_to_output):
   if os.path.isdir(path_to_output):
      os.mkdir(os.path.join(path_to_output, "k-means"))
      # Clusters file
      output_file = open(os.path.join(path_to_output, "k-means", "means-and-clusters.txt"), "w")
      for cluster_index,cluster in enumerate(clusters):
         output_file.write("Cluster#" + str(cluster_index+1) + ":\n")
         for point_index in cluster:
            output_file.write(str(input_data[point_index][0]) + " " + str(input_data[point_index][1]) + "\n")
         output_file.write("Mean:\n")
         output_file.write(str(means[cluster_index][0]) + " " + str(means[cluster_index][1]) + "\n\n")
      output_file.write("Found in " + str(iterations) + " iterations.\n")
      output_file.close()
      # Input file with class labels
      output_file = open(os.path.join(path_to_output, "k-means", "class-labels.txt"), "w")
      for point_index,point in enumerate(input_data):
         output_file.write(str(point[0]) + " " + str(point[1]) + " ")
         for cluster_index,cluster in enumerate(clusters):
            if point_index in cluster:
               output_file.write(str(cluster_index+1) + "\n")
               break
      output_file.close()
   return True

def run(input_data, k, path_to_output):
   # create list that will hold cluster means
   means = list()
   # select the first k elements of input_data as initial cluster means
   for index in range(k):
      means.append(input_data[index])
   # repeat until there are no changes made to means list
   converged = False
   iterations = 0
   while not converged:
      iterations += 1
      # (re-) create clusters 
      clusters = list()
      for index in range(k):
         clusters.append(list())
      # designate each object to cluster with closest mean
      for point_index,point in enumerate(input_data):
         cluster_index = designate_cluster(point, means)
         clusters[cluster_index].append(point_index)
      # recalculate means
      old_means = copy.deepcopy(means) 
      means = recalculate_means(clusters, input_data)
      # compare new means list with old means list
      if cmp(means, old_means) == 0:
         converged = True
   output_results(iterations, means, clusters, input_data, path_to_output)
   return True 

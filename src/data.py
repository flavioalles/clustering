#!/usr/bin/env python 

import os.path

def get(path_to_data):
   # open file pointed by path_to_data
   if os.path.isfile(path_to_data):
      input_data = list()
      input_file = open(path_to_data, "r") 
      for line in input_file:
         point = list()
         for index in range(2):
            point.append(float(line.strip().rstrip("\n").split()[index]))
         input_data.append(point)
      input_file.close()
   else:
      input_data = None
   return input_data

def get_output(path_to_data):
   # open file pointed by path_to_data
   if os.path.isfile(path_to_data):
      input_data = list()
      input_file = open(path_to_data, "r") 
      for line in input_file:
         point = list()
         for index in range(3):
            if index != 2:
               point.append(float(line.strip().rstrip("\n").split()[index]))
            else:
               point.append(int(line.strip().rstrip("\n").split()[index]))
         input_data.append(point)
      input_file.close()
   else:
      input_data = None
   return input_data

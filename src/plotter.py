#!/usr/bin/env python

import data
import matplotlib.pyplot as plt
import numpy as np
import os.path

def main():
   # get output data
   for algorithm in ["k-means", "em"]:
      input_data = data.get_output(os.path.join("results", algorithm, "class-labels.txt"))
      # separate data by cluster
      cluster_one = list()
      cluster_two = list()
      for d in input_data:
         if d[2] == 1:
            cluster_one.append(d[:2])
         else: cluster_two.append(d[:2])
      plt.plot(np.array(cluster_one)[:,0], np.array(cluster_one)[:,1], "bo", np.array(cluster_two)[:,0], np.array(cluster_two)[:,1], "go")
      plt.title(algorithm.upper())
      plt.show()
   return True

if __name__ == "__main__":
   main()

#!/usr/bin/env python

import numpy as np
import random as rd
import math
import os

# globals
MAX_ITERATIONS = 100

def initialize(k, data):
   "Initialize parameters (mu, sigma, pi)"
   # initialize mu randomly
   cols = (data.shape)[1]
   mu = np.zeros((k,cols))
   for row in range(k):
      idx = int(np.floor(rd.random()*len(data)))
      for col in range(cols):
         mu[row][col] += data[idx][col]
   # initialize sigma
   sigma = list()
   for cluster in range(k):
      sigma.append(np.cov(data.T))
   # initialize pi randomly
   sum_pi = 1.0
   pi = np.zeros(k)
   pi += sum_pi/k
   return (mu, sigma, pi)

def likelihood(k, data, mu, sigma, pi):
   """Calculate log likelihood"""
   log_score = 0.0
   for index in range(len(data)):
      log_score += np.log(prior_prob(k, mu, sigma, pi, data[index]))
   return log_score

def prior_prob(k, mu, sigma, pi, data):
   pb = 0.0
   for cluster in range(k):
      pb += pi[cluster]*gdf(data, mu[cluster], sigma[cluster])
   return pb

def gdf(x, mu, sigma):
   score = 0.0
   x_mu = np.matrix(x - mu)
   inv_sigma = np.linalg.inv(sigma)
   det_sqrt = np.linalg.det(sigma)**0.5
   norm_const = 1.0/((2*np.pi)**(len(x)/2)*det_sqrt)
   exp_value = math.pow(math.e,-0.5*(x_mu * inv_sigma * x_mu.T))
   score = norm_const*exp_value
   return score

def e_step(k, mu, sigma, pi, data):
   """Expectation step"""
   gamma_nk = np.zeros((len(data),k))
   for index in range(len(data)):
      for cluster in range(k):
         gamma_nk[index][cluster] = (pi[cluster]*gdf(data[index], mu[cluster], sigma[cluster]))/prior_prob(k, mu, sigma, pi, data[index])
   return gamma_nk

def m_step(k, data, gamma_nk):
   """Maximization step"""
   # calculate new_mu
   N_k = np.zeros(k)
   cols = (data.shape)[1]
   new_mu = np.zeros((k,cols))
   for index in range(k):
      for n in range(len(data)):
         N_k[index] += gamma_nk[n][index]
         new_mu[index] += (gamma_nk[n][index]*data[n])
      new_mu[index] /= N_k[index]
   # calculate new_sigma
   new_sigma = np.zeros((k,cols,cols))
   for cluster in range(k):
      for index in range(len(data)):
         xn = np.zeros((1,cols))
         mun = np.zeros((1,cols))
         xn += data[index]
         mun += new_mu[cluster]
         x_mu = xn - mun
         new_sigma[cluster] += (gamma_nk[index][cluster]*x_mu*x_mu.T)
      new_sigma[cluster] /= N_k[cluster]
   # calculate new_pi
   new_pi = np.zeros(k)
   for index in range(k):
      new_pi[index] += (N_k[index]/len(data))
   return (new_mu, new_sigma, new_pi)

def output_results(iterations, mu, clusters, input_data, path_to_output):
   if os.path.isdir(path_to_output):
      os.mkdir(os.path.join(path_to_output, "em"))
      # Clusters file
      output_file = open(os.path.join(path_to_output, "em", "means-and-clusters.txt"), "w")
      for cluster_index,cluster in enumerate(clusters):
         output_file.write("Cluster#" + str(cluster_index+1) + ":\n")
         for point_index in cluster:
            output_file.write(str(input_data[point_index][0]) + " " + str(input_data[point_index][1]) + "\n")
         output_file.write("Mean:\n")
         output_file.write(str(mu[cluster_index][0]) + " " + str(mu[cluster_index][1]) + "\n\n")
      output_file.write("Found in " + str(iterations) + " iterations.\n")
      output_file.close()
      # Input file with class labels
      output_file = open(os.path.join(path_to_output, "em", "class-labels.txt"), "w")
      for point_index,point in enumerate(input_data):
         output_file.write(str(point[0]) + " " + str(point[1]) + " ")
         for cluster_index,cluster in enumerate(clusters):
            if point_index in cluster:
               output_file.write(str(cluster_index+1) + "\n")
               break
      output_file.close()
   return True

def run(input_data, k, path_to_output):
   # transform input data into numpy array
   data = np.array(input_data)
   # initialize parameters
   mu,sigma,pi = initialize(k, data)
   log_score = likelihood(k, data, mu, sigma, pi)
   threshold = 0.001
   iterations = 0
   while iterations < MAX_ITERATIONS:
      iterations += 1
      # expectation step
      gamma_nk = e_step(k, mu, sigma, pi, data)
      # maximization step
      mu,sigma,pi = m_step(k, data, gamma_nk)
      # evaluate the log likelihood
      new_log_score = likelihood(k, data, mu, sigma, pi)
      # print log_score, new_log_score
      if abs(new_log_score - log_score) < threshold:
         break
      log_score = new_log_score
   # classify data based on gamma_nk
   clusters = list()
   for index in range(k):
      clusters.append(list())
   for index in range(len(input_data)):
      if gamma_nk[index][0] > gamma_nk[index][1]:
         cluster_index = 0
      else: cluster_index = 1
      clusters[cluster_index].append(index)
   # output results
   output_results(iterations, mu, clusters, input_data, path_to_output)
   return True
